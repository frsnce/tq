<?php


namespace AaccB88\Tq\Facade;

use think\Facade;
class Aop extends Facade
{
    protected static function getFacadeClass()
    {
        return 'AaccB88\Tq\AopClient';
    }
}